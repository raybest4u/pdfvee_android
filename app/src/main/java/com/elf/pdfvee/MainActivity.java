package com.elf.pdfvee;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class MainActivity extends AppCompatActivity {
    private PDFView pdfView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        PreferenceManager.init(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        View btn =  findViewById(R.id.btn);
        pdfView = (PDFView) findViewById(R.id.pdfView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");//设置类型，我这里是任意类型，任意后缀的可以这样写。
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(intent, 1);
            }
        });
        Intent intent = getIntent();
        if(intent!=null){
            Uri uri = intent.getData();//得到uri，后面就是将uri转化成file的过程。
            if(uri!=null){
                openPDF(uri);
            }else{
                openHistory();
            }
        }else {
            openHistory();
        }

    }
    private void openHistory(){
        try {
            String path = PreferenceManager.getInstance().getString(KEY_FILE);
            int currentPage = PreferenceManager.getInstance().getInt(KEY_PAGE);
            if (!TextUtils.isEmpty(path)) {
                File pdf = new File(path);
                PDFView.Configurator configurator = pdfView.fromBytes(FileUtil.readFile(pdf));
                configurator.defaultPage(currentPage);
                configurator.onPageChange(onPageChangeListener).onLoad(onLoadCompleteListener);
                configurator.load();
            }
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }
    private final static String KEY_PAGE = "_page";
    private final static String KEY_FILE = "_file";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {//是否选择，没选择就不会继续
            Uri uri = data.getData();//得到uri，后面就是将uri转化成file的过程。
            if(uri!=null)
                openPDF(uri);

        }
    }
    private void openPDF(Uri uri ){


        Log.d("-->", "File Uri: " + uri.toString());
        try {
            String path = FileUtil.getPath(MainActivity.this, uri);

            PreferenceManager.getInstance().setString(KEY_FILE, path);
            File pdf = new File(path);
            PDFView.Configurator configurator = pdfView.fromBytes(FileUtil.readFile(pdf));

            configurator.onPageChange(onPageChangeListener).onLoad(onLoadCompleteListener);
            configurator.load();

        } catch (IOException ie) {
            ie.printStackTrace();
        } catch (URISyntaxException ue) {
            ue.printStackTrace();
        }
    }

    OnPageChangeListener onPageChangeListener = new OnPageChangeListener() {
        @Override
        public void onPageChanged(int page, int pageCount) {
            Log.w("-->","onPageChanged:"+page+"-->"+pageCount);
            PreferenceManager.getInstance().setInt(KEY_PAGE, page);
        }
    };
    OnLoadCompleteListener onLoadCompleteListener = new OnLoadCompleteListener() {
        @Override
        public void loadComplete(int nbPages) {

            int currentPage = PreferenceManager.getInstance().getInt(KEY_PAGE);
            Log.w("-->","loadComplete:"+nbPages);
            Log.w("-->","currentPage:"+currentPage);
            //pdfView.jumpTo(currentPage);
        }
    };
}
